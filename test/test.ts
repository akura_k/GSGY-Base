import GSGY from '../src/index'
import Test from './Test.class'
import Util from '../src/GSGY/Util'

let conn = new GSGY.Database.MySQL.MySQLClient(
    new GSGY.Database.MySQL.MySQLConnection(GSGY.Config.get('MYSQL_CONNECTION'))
)
let arr = new Array(20).fill(0).map(el => {
    let model = new Test()
    model.id = Util.generateUUID()
    model.test = Math.random().toString()
    return model
})

conn.insert(arr)
    .then(function (res) {
        console.log(res.content)
        // wlModel.id = '59f01cbc-1e25-4f08-a91b-faab1259ee20'
        // return conn.select(wlModel)
    })
    // .then(function (res1) {
    //     console.log(res1.content)
    // })