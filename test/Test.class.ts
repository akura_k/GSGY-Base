import GSGY from '../src/index'

export default class Test extends GSGY.Database.SQLModel {
    static table = 'test'
    static primaryKey = 'id'
    id: String | null = null
    test: String | null = null


}