import GSGY from '../src/index';
export default class Test extends GSGY.Database.SQLModel {
    static table: string;
    static primaryKey: string;
    id: String | null;
    test: String | null;
}
//# sourceMappingURL=Test.class.d.ts.map