"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CONST = {
    STATUS: {
        SUCCESS: 'success',
        FAIL: 'fail',
        UNKNOWN: 'unknown'
    },
    ERROR_CODE: {
        UNKNOWN: 'unknown',
        NO_ERROR: 'no_error',
    }
};
/**
 * @define Message 消息通用类
 * @property CONST 常量
 * @property status 响应状态
 * @property errCode 错误编号
 * @property message 提示信息
 * @property content 响应内容
 */
var Message = /** @class */ (function () {
    function Message() {
        // 响应状态
        this.status = CONST.STATUS.UNKNOWN;
        // 错误编号
        this.errCode = CONST.ERROR_CODE.UNKNOWN;
        // 提示信息
        this.message = '';
    }
    // 常量
    Message.STATUS = CONST.STATUS;
    Message.ERROR_CODE = CONST.ERROR_CODE;
    return Message;
}());
exports.default = Message;
//# sourceMappingURL=Message.js.map