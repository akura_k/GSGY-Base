import SQLConnection from '../SQLConnection';
/**
 * @class MySQLConnection MySQL数据连接类
 * @implements SQLConnection
 */
export default class MySQLConnection extends SQLConnection {
    /**
     * @constructor 构造函数
     * @param {Object} config
     * @param {String} config.host 数据库地址
     * @param {Number} [config.port] 端口
     * @default 3306
     * @param {String} config.user 用户
     * @param {String} config.password 密码
     * @param {String} config.database 数据库名
     */
    constructor({ host, port, user, password, database }: {
        host: string;
        port: number;
        user: string;
        password: string;
        database: string;
    });
    /**
     * 连接数据库
     * @override
     */
    connect(): Promise<any>;
    close(): any;
    query(sql: string, valArr?: Array<any>): Promise<any>;
    beginTransaction(): Promise<any>;
    rollback(): Promise<any>;
    commit(): Promise<any>;
    escape(target: string): any;
}
//# sourceMappingURL=MySQLConnection.d.ts.map