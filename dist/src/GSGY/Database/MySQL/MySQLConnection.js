"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var SQLConnection_1 = __importDefault(require("../SQLConnection"));
var mysql2_1 = __importDefault(require("mysql2"));
var Log_1 = __importDefault(require("../../Log"));
/**
 * @class MySQLConnection MySQL数据连接类
 * @implements SQLConnection
 */
var MySQLConnection = /** @class */ (function (_super) {
    __extends(MySQLConnection, _super);
    /**
     * @constructor 构造函数
     * @param {Object} config
     * @param {String} config.host 数据库地址
     * @param {Number} [config.port] 端口
     * @default 3306
     * @param {String} config.user 用户
     * @param {String} config.password 密码
     * @param {String} config.database 数据库名
     */
    function MySQLConnection(_a) {
        var host = _a.host, _b = _a.port, port = _b === void 0 ? 3306 : _b, user = _a.user, password = _a.password, database = _a.database;
        return _super.call(this, { host: host, port: port, user: user, password: password, database: database }) || this;
    }
    /**
     * 连接数据库
     * @override
     */
    MySQLConnection.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, mysql2_1.default.createConnection({
                                host: this.host,
                                port: this.port,
                                database: this.database,
                                user: this.user,
                                password: this.password
                            }).promise()];
                    case 1:
                        _a.connection = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _b.sent();
                        Log_1.default.write(e_1, '数据库连接出错！');
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MySQLConnection.prototype.close = function () {
        return this.connection.close();
    };
    MySQLConnection.prototype.query = function (sql, valArr) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(valArr && valArr.length)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.connection.query(sql, valArr)];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2: return [4 /*yield*/, this.connection.query(sql)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MySQLConnection.prototype.beginTransaction = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.beginTransaction()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MySQLConnection.prototype.rollback = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.rollback()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MySQLConnection.prototype.commit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.commit()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MySQLConnection.prototype.escape = function (target) {
        return this.connection.escape(target);
    };
    return MySQLConnection;
}(SQLConnection_1.default));
exports.default = MySQLConnection;
//# sourceMappingURL=MySQLConnection.js.map