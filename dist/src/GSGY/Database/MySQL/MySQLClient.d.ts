import Message from '../../Model/Message';
import MySQLConnection from './MySQLConnection';
import SQLModel from '../SQLModel';
import SQLClient from '../SQLClient';
/**
 * @class MySQLClient 数据库操作类
 * @property {MySQLConnection} connection 数据库连接对象
 */
export default class MySQLClient extends SQLClient {
    connection: MySQLConnection;
    /**
     * @constructor 构造函数
     * @param {MySQLConnection} connection 数据库连接对象
     */
    constructor(connection: MySQLConnection);
    /**
     * @constructor 构造函数
     * @param {string} connectionKey 配置名称
     */
    constructor(connectionKey: string);
    private _insertSingle;
    /**
     * 新增记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    insert<T extends SQLModel>(modelList: Array<T>): Promise<Message>;
    insert<T extends SQLModel>(model: T): Promise<Message>;
    /**
     * 删除记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    delete<T extends SQLModel>(model: T): Promise<Message>;
    /**
     * 查询单条记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    select<T extends SQLModel>(model: T): Promise<Message>;
    /**
     * 查询记录分页
     * @param {SQLModel} model
     * @param pageIndex
     * @param pageSize
     * @return {Promise<Message>}
     */
    page<T extends SQLModel>(model: T, pageIndex?: number, pageSize?: number): Promise<Message>;
    /**
     * 根据主键修改记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    update<T extends SQLModel>(model: T): Promise<Message>;
    /**
     * 运行SQL语句
     * @param {string} sql
     * @param {Array<string>} [params]
     * @return {Promise<Message>}
     */
    runSQL(sql: string, params?: Array<string>): Promise<Message>;
    /**
     * 连接数据库
     */
    connect(): Promise<any>;
    /**
     * 断开数据库连接
     */
    disconnect(): Promise<any>;
    /**
     * 开始事务
     */
    beginTransaction(): Promise<any>;
    /**
     * 回滚事务
     */
    rollback(): Promise<any>;
    /**
     * 提交事务
     */
    commit(): Promise<any>;
}
//# sourceMappingURL=MySQLClient.d.ts.map