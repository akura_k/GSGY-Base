/**
 * @class SQLModel 数据库模型对象
 */
export default abstract class SQLModel {
    static table: string;
    static primaryKey: string;
    /**
     * 获取表名
     */
    getTable(): any;
    /**
     * 获取主键
     */
    getPrimaryKey(): any;
    /**
     * 反序列化一个列表
     * @param results
     */
    static deserializeList(results: Array<{
        [key: string]: any;
    }>): any[];
    /**
     * 反序列化一个实例
     * @param result
     */
    static deserializeModel(result: {
        [key: string]: any;
    }): any;
    /**
     * 用对象填充Model
     * @param result
     * @return {T extends SQLModel} 实例本身 支持链式调用
     */
    fill(result: {
        [key: string]: any;
    }): this;
}
//# sourceMappingURL=SQLModel.d.ts.map