"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @class SQLConnection 数据库连接类
 * @property {string} host 数据库地址
 * @property {number} port 数据库端口
 * @property {string} user 数据库用户
 * @property {string} password 数据库密码
 * @property {string} database 数据库名称
 * @property {*} connection 数据库连接对象
 */
var SQLConnection = /** @class */ (function () {
    /**
     * @constructor 构造函数
     * @param {Object} config
     * @param {String} config.host 数据库地址
     * @param {Number} [config.port] 端口
     * @param {String} config.user 用户
     * @param {String} config.password 密码
     * @param {String} config.database 数据库名
     */
    function SQLConnection(_a) {
        var host = _a.host, port = _a.port, user = _a.user, password = _a.password, database = _a.database;
        this.host = '';
        this.port = 0;
        this.user = '';
        this.password = '';
        this.database = '';
        this.connection = '';
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.database = database;
    }
    return SQLConnection;
}());
exports.default = SQLConnection;
//# sourceMappingURL=SQLConnection.js.map