"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var lodash_1 = __importDefault(require("lodash"));
/// 读取配置 开始
/**
 * 读取配置
 * @desc 默认读取路径：项目根目录/gsgy.config.js
 */
var CONFIG;
try {
    CONFIG = JSON.parse(fs_1.default.readFileSync(path_1.default.resolve(path_1.default.dirname(((_a = require.main) === null || _a === void 0 ? void 0 : _a.filename) || ''), './gsgy.config.json')).toString());
}
catch (e) {
    CONFIG = {};
}
///! 读取配置 结束
/**
 * 配置模块
 */
var Config = /** @class */ (function () {
    function Config() {
    }
    /**
     * 读取配置
     * @param path 配置名称
     */
    Config.get = function (path) {
        try {
            return lodash_1.default.get(CONFIG, path);
        }
        catch (e) {
            return '';
        }
    };
    return Config;
}());
exports.default = Config;
//# sourceMappingURL=Config.js.map