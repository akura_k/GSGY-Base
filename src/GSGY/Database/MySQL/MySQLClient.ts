import _ from 'lodash'
import Message from '../../Model/Message'
import MySQLConnection from './MySQLConnection'
import SQLModel from '../SQLModel'
import Config from '../../Config'
import SQLClient from '../SQLClient'
import SQLConnection from '../SQLConnection'
import Log from '../../Log'

/**
 * @class MySQLClient 数据库操作类
 * @property {MySQLConnection} connection 数据库连接对象
 */
export default class MySQLClient extends SQLClient {
    connection: MySQLConnection

    /**
     * @constructor 构造函数
     * @param {MySQLConnection} connection 数据库连接对象
     */
    constructor(connection: MySQLConnection)
    /**
     * @constructor 构造函数
     * @param {string} connectionKey 配置名称
     */
    constructor(connectionKey: string)
    constructor(connection: MySQLConnection | string) {
        if (connection instanceof MySQLConnection) {
            super(<SQLConnection>connection)
            this.connection = connection
        } else if (typeof connection == 'string') {
            super(connection)
            this.connection = new MySQLConnection(Config.get(connection))
        } else {
            throw Error('初始化数据库连接参数不正确')
        }
    }


    private async _insertSingle<T extends SQLModel>(model: T) {
        const result = new Message()
        try {
            // 拼接sql model.table
            let sql = `INSERT INTO ${model.getTable()} `
            let keyArr = [], valArr = []
            for (const key in model) {
                if (model.hasOwnProperty(key)) {
                    keyArr.push(key)
                    valArr.push(model[key])
                }
            }

            sql += `(${keyArr.join(',')}) VALUES (${valArr.map(() => '?').join(',')})`
            Log.write('sql _insertSingle/sql: ' + sql)

            // 执行sql
            const [sqlRes] = await this.connection.query(
                sql,
                valArr
            )
            if (sqlRes.affectedRows > 0) {
                result.content = sqlRes
                result.message = '新增数据成功！'
                result.status = Message.STATUS.SUCCESS
            } else {

                Log.write('sql insert/empty: ' + sql)
                result.content = sqlRes
                result.errCode = SQLClient.CONST.ERROR_CODE.INSERT_NOTHING
                result.message = '没有新增任何数据！'
                result.status = Message.STATUS.FAIL
            }
        } catch (e) {
            Log.write(e, 'sql insert/error')
            result.content = e
            result.message = '新增数据失败！'
            result.status = Message.STATUS.FAIL
        }
        return result
    }

    /**
     * 新增记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    async insert<T extends SQLModel>(modelList: Array<T>): Promise<Message>
    async insert<T extends SQLModel>(model: T): Promise<Message>

    async insert<T extends SQLModel>(target: T | Array<T>) {
        const result = new Message()
        const that = this
        try {

            if (_.isArray(target)) {
                for (const el of target) {
                    await that._insertSingle(el)
                }
            } else {
                await that._insertSingle(target)
            }
            // 执行sql
            result.content = await this.connection.commit()
            result.message = '新增数据成功！'
            result.status = Message.STATUS.SUCCESS

        } catch (e) {
            Log.write(e, 'sql insert/error')
            result.content = e
            result.message = '新增数据失败！'
            result.status = Message.STATUS.FAIL
        }
        return result
    }

    /**
     * 删除记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    async delete<T extends SQLModel>(model: T) {
        const result = new Message()
        try {
            const table = model.getTable()
            // 拼接sql
            let sql = `DELETE FROM ${table} `
            let whereArr = [], valArr = []
            for (const key in model) {
                if (model.hasOwnProperty(key) && !_.isNil(model[key])) {
                    whereArr.push(`${key} = ?`)
                    valArr.push(model[key])
                }
            }
            // 数组非空
            if (!_.isEmpty(whereArr)) {
                sql += `WHERE ${whereArr.join(' and ')}`
            }
            Log.write('sql delete/sql: ' + sql)
            // 执行sql
            const [sqlRes] = await this.connection.query(
                sql,
                valArr
            )
            if (sqlRes.affectedRows > 0) {
                result.content = sqlRes
                result.message = '删除数据成功！'
                result.status = Message.STATUS.SUCCESS
            } else {

                Log.write('sql delete/empty: ' + sql)
                result.content = sqlRes
                result.errCode = SQLClient.CONST.ERROR_CODE.DELETE_NOT_FOUND
                result.message = '没有删除任何数据！'
                result.status = Message.STATUS.FAIL
            }
        } catch (e) {
            Log.write(e, 'sql delete/error')
            result.content = e
            result.message = '删除数据失败！'
            result.status = Message.STATUS.FAIL
        }
        return result
    }

    /**
     * 查询单条记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    async select<T extends SQLModel>(model: T) {
        const result = new Message()
        try {
            const table = model.getTable()

            // 拼接sql
            let sql = `SELECT * FROM ${table} `
            let whereArr = [], valArr = []
            for (const key in model) {
                if (model.hasOwnProperty(key) && !_.isNil(model[key])) {
                    whereArr.push(`${key} = ?`)
                    valArr.push(model[key])
                }
            }
            // 数组非空
            if (!_.isEmpty(whereArr)) {
                sql += `WHERE ${whereArr.join(' and ')}`
            }
            Log.write('sql select/sql: ' + sql)
            // 执行sql
            const [sqlRes] = await this.connection.query(
                sql,
                valArr
            )
            if (!_.isEmpty(sqlRes)) {
                result.content = (<any>model.constructor).deserializeList(sqlRes)
                result.message = '查询数据成功！'
                result.status = Message.STATUS.SUCCESS
            } else {
                Log.write('sql select/empty: ' + sql)
                result.content = sqlRes
                result.errCode = SQLClient.CONST.ERROR_CODE.SELECT_NOT_FOUND
                result.message = '没有查询到任何数据！'
                result.status = Message.STATUS.FAIL
            }

        } catch (e) {
            Log.write(e, 'sql select/error')
            result.content = e
            result.message = '查询数据失败！'
            result.status = Message.STATUS.FAIL
        }
        return result
    }

    /**
     * 查询记录分页
     * @param {SQLModel} model
     * @param pageIndex
     * @param pageSize
     * @return {Promise<Message>}
     */
    async page<T extends SQLModel>(model: T, pageIndex: number = 1, pageSize: number = 8) {
        const result = new Message()
        try {
            const table = model.getTable()

            // 拼接sql
            let sql = `SELECT * FROM ${table} `
            let sqlTotal = `SELECT count(1) as total FROM ${table} `
            let whereArr = [], valArr = []
            for (const key in model) {
                if (model.hasOwnProperty(key) && !_.isNil(model[key])) {
                    whereArr.push(`${key} = ?`)
                    valArr.push(model[key])
                }
            }
            // 数组非空
            if (!_.isEmpty(whereArr)) {
                sql += `WHERE ${whereArr.join(' and ')}`
                sqlTotal += `WHERE ${whereArr.join(' and ')}`
            }
            sql += ` limit ${(pageIndex - 1) * pageSize}, ${pageSize}`
            Log.write('sql page/total: ' + sqlTotal)
            // 执行sql
            const [sqlResTotal] = await this.connection.query(
                sqlTotal,
                valArr
            )
            Log.write('sql page/sql: ' + sql)
            // 执行sql
            const [sqlRes] = await this.connection.query(
                sql,
                valArr
            )
            result.content = {
                total: sqlResTotal[0].total,
                data: sqlRes
            }
            result.message = '分页查询数据成功！'
            result.status = Message.STATUS.SUCCESS
        } catch (e) {
            Log.write(e, 'sql page/error')
            result.content = e
            result.message = '分页查询数据失败！'
            result.status = Message.STATUS.FAIL
        }
        return result
    }

    /**
     * 根据主键修改记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    async update<T extends SQLModel>(model: T) {
        const result = new Message()
        try {
            const primaryKey = model.getPrimaryKey()
            const table = model.getTable()
            // 拼接sql
            let sql = `UPDATE ${table} `
            let keyArr = [], valArr = []
            for (const key in model) {
                if (model.hasOwnProperty(key) && key != primaryKey) {
                    keyArr.push(key)
                    valArr.push(model[key])
                }
            }
            if (!_.isEmpty(keyArr)) {
                sql += ` SET ${keyArr.map(el => `${el} = ?`).join(' , ')} `
            }
            sql += ` WHERE ${primaryKey} = ?`
            if (primaryKey in model) {
                valArr.push((<any>model)[model.getPrimaryKey()])
                Log.write('sql update/sql: ' + sql)
                // 执行sql
                result.content = await this.connection.query(
                    sql,
                    valArr
                )
                result.message = '更新数据成功！'
                result.status = Message.STATUS.SUCCESS
            } else {
                Log.write('sql update/primaryKey undefined')
                result.content = {}
                result.message = '更新数据失败，主键未定义！'
                result.status = SQLClient.CONST.ERROR_CODE.PRIMARY_KEY_UNDEFINED
            }
        } catch (e) {
            Log.write(e, 'sql update/error')
            result.content = e
            result.message = '更新数据失败！'
            result.status = Message.STATUS.FAIL
        }
        return result
    }


    /**
     * 运行SQL语句
     * @param {string} sql
     * @param {Array<string>} [params]
     * @return {Promise<Message>}
     */
    async runSQL(sql: string, params?: Array<string>): Promise<Message> {
        const result = new Message()
        try {
            params && params.forEach((param, index) => {
                if (!_.isNil(param)) {
                    sql = sql.replace('${' + index + '}', this.connection.escape(param))
                }
            })
            Log.write('sql runSQL/sql: ' + sql)
            // 执行sql
            const [sqlRes] = await this.connection.query(
                sql
            )
            result.content = sqlRes
            result.message = '运行SQL语句成功！'
            result.status = Message.STATUS.SUCCESS

        } catch (e) {
            Log.write(e, 'sql runSQL/error')
            result.content = e
            result.message = '运行SQL语句失败！'
            result.status = Message.STATUS.FAIL
        }
        return result
    }


    /**
     * 连接数据库
     */
    async connect(){
        return await this.connection.connect()
    }

    /**
     * 断开数据库连接
     */
    async disconnect(){
        return await this.connection.close()
    }


    /**
     * 开始事务
     */
    async beginTransaction(){
        return await this.connection.beginTransaction()
    }
    /**
     * 回滚事务
     */
    async rollback(){
        return await this.connection.rollback()
    }
    /**
     * 提交事务
     */
    async commit(){
        return await this.connection.commit()
    }
}