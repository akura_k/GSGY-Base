/**
 * @class SQLConnection 数据库连接类
 * @property {string} host 数据库地址
 * @property {number} port 数据库端口
 * @property {string} user 数据库用户
 * @property {string} password 数据库密码
 * @property {string} database 数据库名称
 * @property {*} connection 数据库连接对象
 */
export default abstract class SQLConnection {
    host: string = ''
    port: number = 0
    user: string = ''
    password: string = ''
    database: string = ''
    connection: any = ''

    /**
     * @constructor 构造函数
     * @param {Object} config
     * @param {String} config.host 数据库地址
     * @param {Number} [config.port] 端口
     * @param {String} config.user 用户
     * @param {String} config.password 密码
     * @param {String} config.database 数据库名
     */
    protected constructor({host, port, user, password, database}
                              : { host: string, port: number, user: string, password: string, database: string }) {
        this.host = host
        this.port = port
        this.user = user
        this.password = password
        this.database = database
    }

    /**
     * 连接数据库
     */
    abstract async connect(): Promise<any>

    /**
     * 关闭数据库连接
     */
    abstract close(): void
}