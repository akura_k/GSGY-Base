/**
 * @class SQLModel 数据库模型对象
 */
export default abstract class SQLModel {
    static table: string = ''
    static primaryKey: string = ''

    /**
     * 获取表名
     */
    public getTable(){
        return (<any>this.constructor).table
    }

    /**
     * 获取主键
     */
    public getPrimaryKey(){
        return (<any>this.constructor).primaryKey
    }
    /**
     * 反序列化一个列表
     * @param results
     */
    static deserializeList(results: Array<{ [key: string]: any }>) {
        return results.map(row => this.deserializeModel(row))
    }

    /**
     * 反序列化一个实例
     * @param result
     */
    static deserializeModel(result: { [key: string]: any }) {
        const model = new (<any>this)
        return model.fill(result)
    }


    /**
     * 用对象填充Model
     * @param result
     * @return {T extends SQLModel} 实例本身 支持链式调用
     */
    public fill(result: { [key: string]: any }) {
        for (const column in this) {
            if (Object.prototype.hasOwnProperty.call(this, column)) {
                this[column] = result[column]
            }
        }
        return this
    }
}