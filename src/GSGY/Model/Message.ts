const CONST = {
    STATUS: {
        SUCCESS: 'success',
        FAIL: 'fail',
        UNKNOWN: 'unknown'
    },
    ERROR_CODE: {
        UNKNOWN: 'unknown',
        NO_ERROR: 'no_error',
    }
}

/**
 * @define Message 消息通用类
 * @property CONST 常量
 * @property status 响应状态
 * @property errCode 错误编号
 * @property message 提示信息
 * @property content 响应内容
 */
export default class Message {
    // 常量
    static STATUS = CONST.STATUS
    static ERROR_CODE = CONST.ERROR_CODE

    // 响应状态
    status = CONST.STATUS.UNKNOWN

    // 错误编号
    errCode = CONST.ERROR_CODE.UNKNOWN

    // 提示信息
    message = ''

    // 响应内容
    content : any
}
