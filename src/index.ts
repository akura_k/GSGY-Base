import MySQLClient from './GSGY/Database/MySQL/MySQLClient'
import MySQLConnection from './GSGY/Database/MySQL/MySQLConnection'
import SQLConnection from './GSGY/Database/SQLConnection'
import SQLModel from './GSGY/Database/SQLModel'
import SQLClient from './GSGY/Database/SQLClient'
import Message from './GSGY/Model/Message'
import Config from './GSGY/Config'
import Log from './GSGY/Log'
import Util from './GSGY/Util'

export default class GSGY{
    static Database = {
        SQLConnection,
        SQLClient,
        SQLModel,
        MySQL : {
            MySQLConnection,
            MySQLClient
        }
    }
    static Model = {
        Message
    }
    static Config = Config
    static Log = Log
    static Util = Util
}